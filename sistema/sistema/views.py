# -*- coding: utf-8 -*-
from django.contrib.auth import authenticate, login
from django.views.generic import View
from django.shortcuts import render, redirect
from django.http import HttpResponse
import logging

logger = logging.getLogger(__name__)

class Autenticacao(View):

    def get(self, request):
        contexto = {
            'usuario': '',
            'senha': ''
        }
        return render(request, 'autenticacao.html', contexto)

    def post(self, request):
        usuario = request.POST.get('usuario', None)
        senha = request.POST.get('senha', None)

        logger.info('Usuário: {}'.format(usuario))
        logger.info('Senha: {}'.format(senha))

        user = authenticate(request, username=usuario, password=senha)

        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('/livro_caixa')

            return render(request, 'autenticacao.html', {'mensagem': 'Usuário Inativo'})

        return render(request, 'autenticacao.html', {'mensagem': 'Usuário ou senha incorretos'})
