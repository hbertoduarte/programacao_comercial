# -*- coding: utf-8 -*-
from django import forms
from django.forms import DateTimeField
from livro_caixa.models import Entrada, Saida

DATETIME_INPUT_FORMATS = [
    '%d-%m-%Y',              # '25-10-2006'
    '%d/%m/%Y',              # '25/10/2006'
    '%d-%m-%y',              # '25-10-06'
    '%d/%m/%y',              # '25/10/06'
    '%d-%m-%Y %H:%M',        # '25-10-2006 14:30'
    '%d/%m/%Y %H:%M',        # '25/10/2006 14:30'
    '%d-%m-%y %H:%M',        # '25-10-06 14:30'
    '%d/%m/%y %H:%M',        # '25/10/06 14:30'
    '%H:%M %d-%m-%Y',        # '14:30 25-10-2006'
    '%H:%M %d/%m/%Y',        # '14:30 25/10/2006'
    '%H:%M %d-%m-%y',        # '14:30 25-10-06'
    '%H:%M %d/%m/%y',        # '14:30 25/10/06'
    '%d-%m-%Y %H:%M:%S',     # '25-10-2006 14:30:59'
    '%d/%m/%Y %H:%M:%S',     # '25/10/2006 14:30:59'
    '%d-%m-%y %H:%M:%S',     # '25-10-06 14:30:59'
    '%d/%m/%y %H:%M:%S',     # '25/10/06 14:30:59'
    '%H:%M:%S %d-%m-%Y',     # '14:30:59 25-10-2006'
    '%H:%M:%S %d/%m/%Y',     # '14:30:59 25/10/2006'
    '%H:%M:%S %d-%m-%y',     # '14:30:59 25-10-06'
    '%H:%M:%S %d/%m/%y',     # '14:30:59 25/10/06'
]

class FormularioEntrada(forms.ModelForm):

    data = DateTimeField(input_formats=DATETIME_INPUT_FORMATS)

    class Meta:
        model = Entrada
        exclude = []

class FormularioSaida(forms.ModelForm):

    data = DateTimeField(input_formats=DATETIME_INPUT_FORMATS)

    class Meta:
        model = Saida
        exclude = []
