from django.db import models

class Entrada(models.Model):

    valor = models.DecimalField(max_digits=12, decimal_places=3)
    data = models.DateTimeField()
    historico = models.TextField(max_length=300)

    def __str__(self):
        return '{0} : + R$ {1} : {2}'.format(self.data.strftime("%d/%m/%Y %H:%M:%S"), self.valor, self.historico)

    def classe(self):
        return 'entrada';

class Saida(models.Model):

    valor = models.DecimalField(max_digits=12, decimal_places=3)
    data = models.DateTimeField() 
    historico = models.TextField(max_length=300)

    def __str__(self):
        return '{0} : - R$ {1} : {2}'.format(self.data.strftime("%d/%m/%Y %H:%M:%S"), self.valor, self.historico)

    def classe(self):
        return 'saida';
