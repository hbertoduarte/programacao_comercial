from django.apps import AppConfig


class LivroCaixaConfig(AppConfig):
    name = 'livro_caixa'
