# -*- coding: utf-8 -*-
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.shortcuts import render
from livro_caixa.models import Entrada, Saida
from livro_caixa.forms import FormularioEntrada, FormularioSaida
from django.db.models import CharField, BooleanField, Value
from sistema.util import Autenticacao

class LivroCaixaList(Autenticacao, ListView):

    model = Entrada
    context_object_name = 'lista_geral'
    template_name = 'livro_caixa/lista.html'

    def get_queryset(self):
        e = Entrada.objects.all().annotate(
            classe=Value('e', output_field=CharField()),
            entrada=Value(True, output_field=BooleanField()),
            saida=Value(False, output_field=BooleanField()))
        s = Saida.objects.all().annotate(classe=Value('s', output_field=CharField()),
            entrada=Value(False, output_field=BooleanField()),
            saida=Value(True, output_field=BooleanField()))
        return e.union(s).order_by('data')

class NovaEntrada(Autenticacao, CreateView):

    model = Entrada
    form_class = FormularioEntrada
    template_name = 'livro_caixa/nova.html'
    success_url = reverse_lazy('lista-geral')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entrada'] = True
        context['saida'] = False
        return context

class NovaSaida(Autenticacao, CreateView):

    model = Saida
    form_class = FormularioSaida
    template_name = 'livro_caixa/nova.html'
    success_url = reverse_lazy('lista-geral')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entrada'] = False
        context['saida'] = True
        return context

class EditarEntrada(Autenticacao, UpdateView):

    model = Entrada
    form_class = FormularioEntrada
    template_name = 'livro_caixa/editar.html'
    success_url = reverse_lazy('lista-geral')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entrada'] = True
        context['saida'] = False
        return context

class EditarSaida(Autenticacao, UpdateView):

    model = Saida
    form_class = FormularioSaida
    template_name = 'livro_caixa/editar.html'
    success_url = reverse_lazy('lista-geral')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entrada'] = False
        context['saida'] = True
        return context

class ExcluirEntrada(Autenticacao, DeleteView):

    model = Entrada
    template_name = 'livro_caixa/excluir.html'
    success_url = reverse_lazy('lista-geral')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entrada'] = True
        context['saida'] = False
        return context

class ExcluirSaida(Autenticacao, DeleteView):

    model = Saida
    template_name = 'livro_caixa/excluir.html'
    success_url = reverse_lazy('lista-geral')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entrada'] = False
        context['saida'] = True
        return context
