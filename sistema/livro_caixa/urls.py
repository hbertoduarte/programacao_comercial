from django.urls import path
from livro_caixa import views

urlpatterns = [
    path('', views.LivroCaixaList.as_view(), name='lista-geral'),
    path('nova_entrada/', views.NovaEntrada.as_view(), name='nova-entrada'),
    path('nova_saida/', views.NovaSaida.as_view(), name='nova-saida'),
    path('entrada/<int:pk>/', views.EditarEntrada.as_view(), name='editar-entrada'),
    path('saida/<int:pk>/', views.EditarSaida.as_view(), name='editar-saida'),
    path('excluir_entrada/<int:pk>/', views.ExcluirEntrada.as_view(), name='excluir-entrada'),
    path('excluir_saida/<int:pk>/', views.ExcluirSaida.as_view(), name='excluir-saida'),
]
