from django.contrib.auth.mixins import LoginRequiredMixin

class Autenticacao(LoginRequiredMixin):

    login_url = '/'
